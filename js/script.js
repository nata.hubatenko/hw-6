function createNewUser() {
	let userName = prompt('What is your first name?');
	let userSurname = prompt('What is your last name?');
	let userBirthday = prompt("Please enter year of birth:'yyyy.mm.dd'");

	function formatDate(date) {
		let dd = date.getDate();
		if (dd < 10) dd = '0' + dd;

		let mm = date.getMonth() + 1;
		if (mm < 10) mm = '0' + mm;

		let yy = date.getFullYear();
		if (yy < 10) yy = '0' + yy;

		return dd + '.' + mm + '.' + yy;
	}
	let d = new Date(userBirthday);
	const newUser = {
		firstName: userName,
		lastName: userSurname,
		birthday: formatDate(d),
		getLogin: function () {
			return (this.firstName.charAt(0) + this.lastName).toLowerCase();
		},
		getAge: function () {
			const currentDate = new Date();
			const currentYear = currentDate.getFullYear();
			const yearOfBirth = d.getFullYear();
			let userAge
			userAge = currentYear - yearOfBirth;
			return userAge;
		},
		getPassword: function () {
			const yearOfBirth = d.getFullYear();
			return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + yearOfBirth;
		}
	};
	return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());









